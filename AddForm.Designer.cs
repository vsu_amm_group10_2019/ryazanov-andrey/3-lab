﻿
namespace WorkWithHash
{
    partial class AddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.AddID = new System.Windows.Forms.TextBox();
            this.Addfio = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.AddGroup = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID";
            // 
            // AddID
            // 
            this.AddID.Location = new System.Drawing.Point(107, 56);
            this.AddID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.AddID.Name = "AddID";
            this.AddID.Size = new System.Drawing.Size(157, 22);
            this.AddID.TabIndex = 1;
            // 
            // Addfio
            // 
            this.Addfio.Location = new System.Drawing.Point(107, 91);
            this.Addfio.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Addfio.Name = "Addfio";
            this.Addfio.Size = new System.Drawing.Size(157, 22);
            this.Addfio.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "FIO";
            // 
            // AddGroup
            // 
            this.AddGroup.Location = new System.Drawing.Point(107, 134);
            this.AddGroup.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.AddGroup.Name = "AddGroup";
            this.AddGroup.Size = new System.Drawing.Size(157, 22);
            this.AddGroup.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Group";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(107, 182);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 30);
            this.button1.TabIndex = 10;
            this.button1.Text = "Do";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // AddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 237);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AddGroup);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Addfio);
            this.Controls.Add(this.AddID);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "AddForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Car";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox AddID;
        private System.Windows.Forms.TextBox Addfio;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox AddGroup;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
    }
}