﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorkWithHash
{
    public partial class Form1 : Form
    {
        private string FileName{ get; set;}
        SquareHashTable SquareHash { get; set; } = new SquareHashTable(100);
        
        public Form1()
        {
            InitializeComponent();
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm(FormState.ADD);
            addForm.ShowDialog();
            if (addForm.ok)
            {
                if (SquareHash.Add(addForm.Car))
                {
                    Redraw();
                }
                else { MessageBox.Show("Record not added"); }
           }
        }

        private void Redraw()
        {
            List<Car> cars = SquareHash.GetData();
            TableCar.Rows.Clear();
            TableCar.RowCount = cars.Count;
            for(int i = 0; i < cars.Count; i++)
            {
                TableCar.Rows[i].Cells[0].Value = cars[i].ID;
                TableCar.Rows[i].Cells[1].Value = cars[i].FIO;
                TableCar.Rows[i].Cells[2].Value = cars[i].Group;
            }
        }

        private void editToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.SEARCH);
            search.ShowDialog();
            if (search.ok)
            {
                string id = search.Car.ID;
                Car car = SquareHash.Find(id);            
                if (car != null)
                {
                    AddForm edit = new AddForm(FormState.EDIT, car);
                    edit.ShowDialog();
                if (edit.ok)
                {
                    SquareHash.Delete(id);
                    if (SquareHash.Add(edit.Car))
                    {
                        Redraw();
                    }
                    else
                    {
                        MessageBox.Show("Record not added");
                    }
                }
                }
                else
                {
                    MessageBox.Show("Car not found");
                }
           }
        }

        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.SEARCH);
            search.ShowDialog();
            if (search.ok)
            {
                string id = search.Car.ID;
                Car car = SquareHash.Find(id);
                if (car != null)
                {
                    AddForm show = new AddForm(FormState.DISPLAY, car);
                    show.ShowDialog();

                }
                else
                {
                    MessageBox.Show("Car not found");
                }
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.SEARCH);
            search.ShowDialog();
            if (search.ok)
            {
                string id = search.Car.ID;
                SquareHash.Delete(id);
                Redraw();
            }
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                SaveToFile();
            }
            openFileDialog.Filter = "json file |*.json";
            openFileDialog.ShowDialog();
            if (!string.IsNullOrEmpty(openFileDialog.FileName))
            {
                FileName = openFileDialog.FileName;
                string txt = File.ReadAllText(FileName);
                List<Car> cars = JsonConvert.DeserializeObject<List<Car>>(txt);
                SquareHash.Clear();
                foreach (Car car in cars)
                {
                    SquareHash.Add(car);
                }
                Redraw();
            }
        }

        private void newFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(FileName))
            {
                SaveToFile();
            }
            FileName = "";
            SquareHash.Clear();
            Redraw();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "json file |*.json";
            saveFileDialog.ShowDialog();
            if (!string.IsNullOrEmpty(saveFileDialog.FileName))
            {
                FileName = saveFileDialog.FileName;
                SaveToFile();
            }
        }

        private void saveFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                SaveToFile();
            }
            else
            {
                saveAsToolStripMenuItem_Click(sender, e);
            }
        }
        private void SaveToFile()
        {
            List<Car> cars = SquareHash.GetData();
            string result = JsonConvert.SerializeObject(cars);
            File.WriteAllText(FileName, result);
        }

    }
}
