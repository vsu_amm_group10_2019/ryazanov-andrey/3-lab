﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkWithHash
{
    class SquareHashTable : IHashTable
    {
        private Elem[] Table { get; }
        const int c = 15, d = 14;
        public int Size { get; }
        public SquareHashTable(int size = 100) // Конструктор
        {
            Size = size;
            Table = new Elem[size];
        }
        public bool Add(Car car) //Добавление данных
        {
            if (Find(car.ID) != null)
            {
                return false;
            }
            int hash = Car.Hash(car.ID);
            int index = hash % Size;
            int i = 0;
            while (i < Size && Table[index] != null)
            {
                i++;
                index = (hash + c * i + d * i * i) % Size;
            }
            if (Table[index] == null || Table[index].Deleted)
            {
                Table[index] = new Elem();
                {
                   Table[index].Car = car;
                };
            }
            return true;
        }

        public Car Find(string id) //Поиск данных
        {
            int hash = Car.Hash(id);
            int index = hash % Size;
            int i = 0;
 
            while (Table[index] != null )
            {
                if (!Table[index].Deleted && Table[index].Car.ID == id)
                {
                    return Table[index].Car;
                }
                else i++;
                index = (hash + c * i + d * i * i) % Size;
            }
            return null;
       }

        public void Delete (string id) //Удаление данных
        {
            int hash = Car.Hash(id);
            int index = hash % Size;
            int i = 0;
            if (Table[index] == null)
            {
                return;
            }
            while (Table[index] != null || Table[index].Deleted)
            {
                if (!Table[index].Deleted && Table[index].Car.ID == id)
                {
                    Table[index].Deleted = true;
                    Table[index].Car = null;
                    return;
                }
                i++;
                index = (hash + c * i + d * i * i) % Size;
            }     
         }
        


        public void Clear() //Очистка
        {
            for (int i = 0; i < Size; i++)
            {
                Table[i] = null;
            }
        }

        public List<Car> GetData()
        {
            List < Car > list = new List<Car>();
            foreach(Elem e in Table)
            {
                if(e != null && !e.Deleted)
                {
                    list.Add(e.Car);
                }
            }
            return list;
        }
    }
  class Elem
  {
      public Car Car { get; set; }
      public bool Deleted { get; set; } = false; 
   }
   
}
