﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkWithHash
{
    interface IHashTable
    {
        bool Add(Car car);
        Car Find(string id);
        void Delete(string id);
        List<Car> GetData(); 
        void Clear();
    }
}
