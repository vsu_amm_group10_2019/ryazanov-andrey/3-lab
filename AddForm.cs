﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorkWithHash
{
    public partial class AddForm : Form
    {
        public Car Car { get; } = new Car();
        private FormState FormState;
        public AddForm(FormState formState, Car car = null)
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MinimizeBox = false;
            MaximizeBox = false;
            FormState = formState;
            switch (formState)
            {
                case FormState.ADD:
                    {
                        return;
                    }
                case FormState.EDIT:
                    {
                        AddID.ReadOnly = true;
                        AddID.Text = car.ID;
                        Addfio.Text = car.FIO;
                        AddGroup.Text = car.Group;
                        Text = "Edit car";
                        break;
                    }
                case FormState.DELETE:
                   
                case FormState.SEARCH:
                    {
                        AddGroup.ReadOnly = true;
                        Addfio.ReadOnly = true;
                        Text = "Search car";
                        break;
                    }

                case FormState.DISPLAY:
                    {
                        AddID.ReadOnly = true;
                        AddID.Text = car.ID;
                        Addfio.Text = car.FIO;
                        AddGroup.Text = car.Group;

                        AddGroup.ReadOnly = true;
                        Addfio.ReadOnly = true;
                        Text = "Show car";
                        break;
                    }
                    
            }
        }

        public bool ok;
        private void button1_Click(object sender, EventArgs e)
        {
            if (FormState == FormState.ADD || FormState == FormState.DELETE || FormState == FormState.SEARCH || FormState == FormState.EDIT)
            {
                if (AddID.Text != "")
                {
                    Car.ID = AddID.Text;
                    ok = true;
                    this.Close();
                }
                else
                {
                    MessageBox.Show(
                            "ID not specified!",
                            "Error!",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                }

            }

            if (FormState == FormState.ADD || FormState == FormState.EDIT)
            {
                if (Addfio.Text != ""  && AddGroup.Text != "" && AddID.Text != "")
                {
                    Car.FIO = Addfio.Text;
                    Car.Group = AddGroup.Text;
                    ok = true;
                    this.Close();
                }
                else
                {
                    ok = false; 
                    MessageBox.Show(
                              "Not all fields are filled in!",
                              "Error!",
                              MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
                }


                if (FormState == FormState.DISPLAY)
                {
                    ok = true;
                    this.Close();
                }

            }

        }


    }
}
